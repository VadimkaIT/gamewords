﻿using System;
using System.Runtime.Serialization;

namespace Words
{
    [DataContract]
    class Score
    {
        [DataMember]
        public Player Player1 { get; set; }

        [DataMember]
        public Player Player2 { get; set; }

        [DataMember]
        public int ScorePlayer1 { get; set; }

        [DataMember]
        public int ScorePlayer2 { get; set; }

        public Score() 
        {
            Player1 = new Player();
            Player2 = new Player();
        }

        public Score(Player player1, Player player2, int scorePlayer1, int scorePlayer2)
        {
            Player1 = player1;
            Player2 = player2;
            ScorePlayer1 = scorePlayer1;
            ScorePlayer2 = scorePlayer2;
        }

        public Score(Player player1, Player player2)
        {
            Player1 = player1;
            Player2 = player2;
        }

        public override string ToString()
        {
            return Player1.ToString() + " " + ScorePlayer1.ToString() + ":" + ScorePlayer2.ToString() + " " + Player2.ToString();
        }
    }
}
