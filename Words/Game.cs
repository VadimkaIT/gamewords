﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Words
{
    class Game
    {
        public Player Player1 { get; set; }
        public Player Player2 { get; set; }

        private string _mainWord;
        public string MainWord 
        {
            get => _mainWord;
            set
            {
                if (value.Length >= 8 && value.Length <= 30 && Regex.IsMatch(value, @"[а-яА-Яa-zA-z]+"))
                {
                    _mainWord = value;
                }
            }
        }
        public List<string> ListWords { get; set; }
        public string Lang { get; set; }
        public Stopwatch Duration { get; set; }
        public List<Score> ListAllScores { get; set; }
        public int Move { get; set; }

        private Timer _timer;

        public Game()
        {
            Player1 = new Player();
            Player2 = new Player();
            ListWords = new List<string>();
            Duration = new Stopwatch();
            Lang = "ru";
            Move = 1;

            var jsonFormatter = new DataContractJsonSerializer(typeof(List<Score>));
            using (var file = new FileStream("score.json", FileMode.OpenOrCreate))
            {
                /**
                 * Reading data from the file to the list.
                 * If an error occurred while reading, create an empty list.
                 */
                try
                {
                    ListAllScores = jsonFormatter.ReadObject(file) as List<Score>;
                }
                catch (Exception e)
                {
                    ListAllScores = new List<Score>();
                }               
            }
        }

        public void ChooseLanguage()
        {
            Console.WriteLine("Выберите язык:");
            Console.WriteLine("1. Русский");
            Console.WriteLine("2. Английский");
            Console.Write("---> ");

            string lang = Console.ReadLine();
            switch (lang)
            {
                case "1":
                    lang = "ru";
                    break;
                case "2":
                    lang = "en";
                    break;
                default:
                    Console.Clear();
                    ChooseLanguage();
                    return;
            }

            Console.Clear();

            Lang = lang;
        }

        public void InputNicknames()
        {
            string playerName1;
            string playerName2;

            switch (Lang)
            {
                case "ru":
                    Console.WriteLine("Введите ваши никнеймы:");
                    Console.Write("\nИгрок 1: ");
                    playerName1 = Console.ReadLine().Trim();
                    Console.Write("\nИгрок 2: ");
                    playerName2 = Console.ReadLine().Trim();
                    break;
                case "en":
                    Console.WriteLine("Enter your nicknames:");
                    Console.Write("\nPlayer 1: ");
                    playerName1 = Console.ReadLine().Trim();
                    Console.Write("\nPlayer 2: ");
                    playerName2 = Console.ReadLine().Trim();
                    break;
                default:
                    playerName1 = "";
                    playerName2 = "";
                    break;
            }

            if (playerName1 == playerName2)
            {
                switch (Lang)
                {
                    case "ru":
                        Console.WriteLine("\nНикнеймы должны быть различны!");
                        Console.WriteLine("Попробуйте еще раз!\n");
                        break;
                    case "en":
                        Console.WriteLine("\nNicknames must be different!");
                        Console.WriteLine("Try again!\n");
                        break;
                }
                InputNicknames();
                return;
            }

            Player1.Name = playerName1;
            Player2.Name = playerName2;

            Console.Clear();
        }

       public void InputMainWord()
        {
            string word;
            string rule = @"[а-яА-Яa-zA-z]+";

            while (true)
            {
                switch (Lang)
                {
                    case "ru":
                        Console.WriteLine("Введите начальное слово (8-30 символов).");
                        break;
                    case "en":
                        Console.WriteLine("Enter the main word (8-30 characters).");
                        break;
                }

                word = Console.ReadLine().Trim();    
                if (word.Length >= 8 && word.Length <= 30 && Regex.IsMatch(word, rule))
                {
                    MainWord = word;
                    ListWords.Add(MainWord.ToLower());
                    break;
                }

                Console.Clear();
            }
        }

        public void ShowWords()
        {
            switch (Lang)
            {
                case "ru":
                    Console.WriteLine("\nВведенные слова:");
                    break;
                case "en":
                    Console.WriteLine("\nEntered words:");
                    break;
            }

            foreach (var word in ListWords)
            {
                Console.WriteLine(word);
            }

            Console.WriteLine();
        }

        public void ChangeMove() => Move = (Move == 1 ? 2 : 1);

        public void Start()
        {
            Duration.Start();

            while (true)
            {
                string word;

                while (true)
                {
                    switch (Lang)
                    {
                        case "ru":
                            Console.WriteLine("Список команд:");
                            Console.WriteLine("/show-words – показать все введенные слова в текущей игре.");
                            Console.WriteLine("/score – показать общий счет по играм для текущих игроков.");
                            Console.WriteLine("/total-score – показать общий счет для всех игроков.");
                            Console.WriteLine($"\nНачальное слово: {MainWord}");
                            break;
                        case "en":
                            Console.WriteLine("Command List:");
                            Console.WriteLine("/show-words – show all entered words in the current game.");
                            Console.WriteLine("/score – show total game score for current players.");
                            Console.WriteLine("/total-score – show total score for all players.");
                            Console.WriteLine($"\nInitial word: {MainWord}");
                            break;
                    }

                    string playerName;
                    if (Move == 1)
                    {
                        playerName = Player1.Name;
                    }
                    else
                    {
                        playerName = Player2.Name;
                    }
                    Console.Write($"\n{playerName}: ");

                    TimerCallback tmcb = new TimerCallback(CallTimer);
                    if (_timer == null)
                    {
                        _timer = new Timer(tmcb, null, 30000, 30000);
                    }

                    word = Console.ReadLine().ToLower();
                    bool isCommand = true;
                    switch (word)
                    {
                        case "/show-words":
                            ShowWords();
                            break;
                        case "/score":
                            ShowScore();
                            break;
                        case "/total-score":
                            ShowAllScores();
                            break;
                        default:
                            isCommand = false;
                            break;
                    }

                    if (!isCommand)
                    {
                        break;
                    }
                }

                // If the entered word contains more than just letters – finish the game.
                if (!Regex.IsMatch(word, @"^[a-zа-я]+$")) 
                {
                    Duration.Stop();
                    Result();
                }

                /**
                 * If the length of the entered word doesn't exceed the length of the main word,
                 * starting checking the word for correctness.
                 * Otherwise – finish the game.
                 */
                if (word.Length <= MainWord.Length)
                {
                    /**
                     * Checking whether the entered word consists only of the letters of the main word 
                     * (taking into account the number of letters).
                     * We group the letters and check that the number of any letter in the entered word 
                     * doesn't exceed the number of this letter in the main word.
                     */
                    bool isCorrectWord = word
                        .GroupBy(g => g)
                        .All(x => x.Count() <= MainWord.Count(c => c == x.Key));

                    if (isCorrectWord && !ListWords.Contains(word))
                    {
                        _timer.Dispose();
                        _timer = null;
                        ListWords.Add(word);
                        ChangeMove();
                    }
                    else
                    {
                        Duration.Stop();
                        Result();
                    }
                }
                else
                {
                    Duration.Stop();
                    Result();
                }
            }
        }

        private void CallTimer(object obj)
        {
            Result();
        }

        public void Result()
        {
            _timer.Dispose();

            string playerName;
            if (Move == 1)
            {
                playerName = Player1.Name;
            }
            else
            {
                playerName = Player2.Name;
            }

            switch (Lang)
            {
                case "ru":
                    Console.WriteLine($"\nИгрок {playerName} проиграл");
                    break;
                case "en":
                    Console.WriteLine($"\nPlayer {playerName} lost");
                    break;
            }

            WriteTotalScore(); 

            TimeSpan ts = Duration.Elapsed;

            // Format and display the TimeSpan value.
            string elapsedTime = String.Format("{0:00}:{1:00}.{2:00}",
                ts.Minutes, ts.Seconds,
                ts.Milliseconds / 10);
            switch (Lang)
            {
                case "ru":
                    Console.WriteLine($"\nИгра длилась {elapsedTime} минут");
                    break;
                case "en":
                    Console.WriteLine($"\nThe game lasted {elapsedTime} minutes");
                    break;
            }
            Console.ReadKey();
            
            Environment.Exit(0);
        }

        // Show the total score of current players.
        public void ShowScore()
        {
            bool isFound = false;

            // Looking for information about joint games of current players in the list.
            foreach (var score in ListAllScores)
            {
                if ((score.Player1.Name == Player1.Name && score.Player2.Name == Player2.Name) ||
                    (score.Player1.Name == Player2.Name && score.Player2.Name == Player1.Name))
                {
                    Console.WriteLine(score);
                    isFound = true;
                    break;
                }
            }

            if (!isFound)
            {
                Console.WriteLine(Player1.Name + " " + 0 + ":" + 0 + " " + Player2.Name);
            }

            Console.WriteLine();
        }

        // Show the total score of all players.
        public void ShowAllScores()
        {
            foreach (var score in ListAllScores)
            { 
                Console.WriteLine(score);
            }

            Console.WriteLine();
        }

        public void WriteTotalScore()
        {
            bool isFound = false;

            /** 
             * Looking for information about joint games of current players in the list.
             * Change the score depending on the victory of a particular player.
             */
            foreach (var score in ListAllScores)
            {
                if (score.Player1.Name == Player1.Name && score.Player2.Name == Player2.Name)
                {
                    if (Move == 1)
                    {
                        score.ScorePlayer2++;
                    }
                    else
                    {
                        score.ScorePlayer1++;
                    }
                    isFound = true;
                    break;
                }
                else if (score.Player1.Name == Player2.Name && score.Player2.Name == Player1.Name)
                {
                    if (Move == 1)
                    {
                        score.ScorePlayer1++;
                    }
                    else
                    {
                        score.ScorePlayer2++;
                    }
                    isFound = true;
                    break;
                }
            }

            if (!isFound)
            {
                Score score = new Score(Player1, Player2);
                if (Move == 1)
                {
                    score.ScorePlayer2++;
                }
                else
                {
                    score.ScorePlayer1++;
                }
                ListAllScores.Add(score);
            }

            // Writing the result of all games to the file.
            var jsonFormatter = new DataContractJsonSerializer(typeof(List<Score>));
            using (var file = new FileStream("score.json", FileMode.Create))
            {
                jsonFormatter.WriteObject(file, ListAllScores);
            }
        }

        public void ShowRules ()
        {
            switch (Lang)
            {
                case "ru":
                    Console.WriteLine("Правила игры:\nВ начале игры пользователь вводит слово определенной длины: " +
                        "суть игры заключается в том, чтобы 2 пользователя поочередно вводили слова, " +
                        "состоящие из букв первоначально указанного слова. Проигрывает тот, кто в свою " +
                        "очередь не вводит слово. На ввод слова дается 30 секунд.");
                    break;
                case "en":
                    Console.WriteLine("Rules of the game:\nAt the beginning of the game, the user enters a word of a certain " +
                        "length: the essence of the game is that 2 users alternately enter words consisting " +
                        "of the letters of the originally specified word. " +
                        "The player who doesn't enter a word – loses. You have 30 seconds to enter a word.");
                    break;
            }
            Console.ReadKey();
            Console.Clear();
        }
    }
}
