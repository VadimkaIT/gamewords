﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection.Emit;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Json;
using System.Text.RegularExpressions;
using System.Threading;

namespace Words
{
    class Program
    {
        #region unmanaged

        // Declare the SetConsoleCtrlHandler function
        // as external and receiving a delegate.

        [DllImport("Kernel32")]
        public static extern bool SetConsoleCtrlHandler(HandlerRoutine Handler, bool Add);


        // A delegate type to be used as the handler routine
        // for SetConsoleCtrlHandler.
        public delegate bool HandlerRoutine(CtrlTypes CtrlType);



        // An enumerated type for the control messages
        // sent to the handler routine.

        public enum CtrlTypes
        {

            CTRL_C_EVENT = 0,
            CTRL_BREAK_EVENT,
            CTRL_CLOSE_EVENT,
            CTRL_LOGOFF_EVENT = 5,
            CTRL_SHUTDOWN_EVENT

        }

        #endregion

        private static Game game;
        static void Main(string[] args)
        {
            game = new Game();
            game.ChooseLanguage();
            game.ShowRules();
            game.InputNicknames();
            game.InputMainWord();
            SetConsoleCtrlHandler(new HandlerRoutine(ConsoleCtrlCheck), true);
            game.Start();            
        }
        private static bool ConsoleCtrlCheck(CtrlTypes ctrlType)
        {
            if (ctrlType == CtrlTypes.CTRL_C_EVENT || ctrlType == CtrlTypes.CTRL_BREAK_EVENT ||
                ctrlType == CtrlTypes.CTRL_CLOSE_EVENT || ctrlType == CtrlTypes.CTRL_LOGOFF_EVENT ||
                ctrlType == CtrlTypes.CTRL_SHUTDOWN_EVENT)
            {
                game.WriteTotalScore();
                Environment.Exit(0);
            }    

            return true;
        }
    }
}